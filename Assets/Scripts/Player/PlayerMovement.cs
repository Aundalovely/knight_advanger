﻿using Enemy;
using Sound;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private float playerSpeed;
        [SerializeField] private float jumpSpeed;
        [SerializeField] private float attackDamage;

        public bool isJumping;
        private bool isDashing;
        

        private float[] attackDetails = new float[2];
        private float move;
        private float dashTimeLeft;
        private float lastImageXpos;
        private float lastDash = -100f;

        private int facingDirection;
    
        public Transform attackPoint;
        public LayerMask enemyLayers;
        public float attackRange = 0.5f;
        public float dashTime;
        public float dashSpeed;
        public float distanceBetweenImages;
        public float dashCoolDown;
        private Rigidbody2D rb;
        private Animator anim;
        

        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
        }

        private void Update()
        {
            CheckInput();
            Animation();
            CheckDash();
        }
    

        void Animation()
        {
            anim.SetFloat("Movement", Mathf.Abs(move));
            anim.SetBool("isJumping",isJumping);
        }

    

        private void CheckInput()
        {
            move = Input.GetAxis("Horizontal");
        
            rb.velocity = new Vector2(move * playerSpeed, rb.velocity.y);

            if (move < 0)
            {
                transform.eulerAngles = new Vector3(0,180,0);
                facingDirection = -1;
            }
            else if (move > 0)
            {
                transform.eulerAngles = new Vector3(0,0,0);
                facingDirection = 1;
            }

            if (Input.GetButtonDown("Jump") && !isJumping)
            {
                SoundManager.PlaySound("jump");
                rb.AddForce(new Vector2(rb.velocity.x,jumpSpeed));
                isJumping = true;
            }

            if (Input.GetButtonDown("Dash"))
            {
                if (Time.time >= (lastDash + dashCoolDown))
                {
                    SoundManager.PlaySound("dash");
                    AttemptToDash();
                }
            }
 
            if (Input.GetMouseButtonDown(0))
            {
                SoundManager.PlaySound("click");
                Attack();
            }
        }

        private void AttemptToDash()
        {
            isDashing = true;
            dashTimeLeft = dashTime;
            lastDash = Time.time;

            PlayerAfterImagePool.Instance.GetFromPool();
            lastImageXpos = transform.position.x;
        }
    
        private void CheckDash()
        {
            if (isDashing)
            {
                if(dashTimeLeft > 0)
                {
                    rb.velocity = new Vector2(dashSpeed * facingDirection, rb.velocity.y);
                    dashTimeLeft -= Time.deltaTime;

                    if (Mathf.Abs(transform.position.x - lastImageXpos) > distanceBetweenImages)
                    {
                        PlayerAfterImagePool.Instance.GetFromPool();
                        lastImageXpos = transform.position.x;
                    }
                }

                if(dashTimeLeft <= 0)
                {
                    isDashing = false;
                }
            
            }
        }
        

        private void Attack()
        {
            anim.SetTrigger("Attack");
            Collider2D[] hitEnemies =Physics2D.OverlapCircleAll(attackPoint.position,attackRange,enemyLayers);
        
            attackDetails[0] = attackDamage;
            attackDetails[1] = transform.position.x;


            foreach (Collider2D enemy in hitEnemies)
            {
                SoundManager.PlaySound("hit");
                enemy.transform.parent.SendMessage("Damage", attackDetails);
                Debug.Log("We hit " + enemy.name);
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "FallDetection")
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }


        private void OnDrawGizmosSelected()
        {
            if (attackPoint == null)
            {
                return;
            }
            Gizmos.DrawWireSphere(attackPoint.position,attackRange);
        }
        
    }
}
