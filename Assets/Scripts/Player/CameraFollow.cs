﻿using UnityEngine;

namespace Player
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField] private GameObject player;
        [SerializeField] private float timeOfSet;
        [SerializeField] private Vector2 posOffSet;
        private Vector3 velocity;
        private void Update()
        {
            Vector3 startPos = transform.position;
            Vector3 endPos = player.transform.position;
            endPos.x += posOffSet.x;
            endPos.y += posOffSet.y;
            endPos.z = -10;
        
            transform.position = Vector3.Lerp(startPos,endPos,timeOfSet * Time.deltaTime);
        
        
        }
    }
}
