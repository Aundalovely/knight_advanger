﻿using UnityEngine;

namespace Player
{
    public class Feet : MonoBehaviour
    {
        private PlayerMovement player;

        private void Start()
        {
            player = GetComponentInParent<PlayerMovement>();
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Ground") && player.isJumping)
            {
                player.isJumping = false;
            }
        }
    }
}
