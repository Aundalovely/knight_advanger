﻿using System;
using Sound;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameManager
{
    public class GameManager : MonoBehaviour
    {
        private bool isPaused = false;
        [SerializeField] private GameObject finishUI;
        [SerializeField] private GameObject pauseUI;
        private int collectedScore;
        [SerializeField]private int victoryCondition;
        private static GameManager instance;
        private void Awake()
        {
            if (instance == null )
            {
                instance = this;
            }

            else
            {
                Destroy(this);
            }
        }

        public static GameManager MyInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameManager();
                }
                return instance;
            }
        }

        private void Start()
        {
            UIManager.UIManager.MyInstance.SoreUI(collectedScore,victoryCondition);
        }

        public void AddScore(int _score)
        {
            collectedScore += _score;
            UIManager.UIManager.MyInstance.SoreUI(collectedScore,victoryCondition);
        }

        public void LevelTwo()
        {
            if (collectedScore >= victoryCondition)
            {
                SceneManager.LoadScene("Level2");
            }
            else
            {
                UIManager.UIManager.MyInstance.ShowVictoryCondition(collectedScore,victoryCondition);
            }
        }

        public void Finish()
        {
            if (collectedScore >= victoryCondition)
            {
                SoundManager.PlaySound("finishLevel");
                finishUI.SetActive(true);
                Time.timeScale = Time.timeScale == 1 ? 0 : 1;
                isPaused = isPaused = true ? false : true;
            }
            else
            {
                UIManager.UIManager.MyInstance.ShowVictoryCondition(collectedScore,victoryCondition);
            }
        }
        

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SwitchPause();
            }
        }

        public void SwitchPause()
        {
            pauseUI.SetActive(!pauseUI.activeSelf);
            Time.timeScale = Time.timeScale == 1 ? 0 : 1;
            isPaused = isPaused = true ? false : true;
        }

        public void Menu()
        {
            SceneManager.LoadScene("MainMenu");
            Time.timeScale = Time.timeScale == 1 ? 0 : 1;
            isPaused = isPaused = true ? false : true;
        }

        public void Exit()
        {
            Debug.Log("Exit");
            Application.Quit();
        }

        public void PlayAgain()
        {
            SceneManager.LoadScene("Level1");
            Time.timeScale = Time.timeScale == 1 ? 0 : 1;
            isPaused = isPaused = true ? false : true;
        }
    }
}
