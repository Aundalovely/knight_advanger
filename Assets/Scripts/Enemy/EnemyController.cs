﻿using System;
using Sound;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        private enum State
        {
            Walking,
            Knockback,
            Dead
        }

        private State currentState;

        [SerializeField] private float groundCheckDistance;
        [SerializeField] private float movementSpeed;
        [SerializeField] private float maxHp;
        [SerializeField] private float knockBackDuration;
        
        [SerializeField] private Transform groundCheck;
        [SerializeField] private LayerMask isGround;
        [SerializeField] private Vector2 knockBackSpeed;
        [SerializeField] private int scoreValue = 1;
        
        private float currentHp;
        private float knockbackStartTime;

        private int facingDirection;
        private int damageDirection;

        private Vector2 movement;
        private Vector2 touchDamageBotLeft;
        private Vector2 touchDamageTopRight;

        private bool groundDetected;
    
        private GameObject alive;
        [SerializeField] private GameObject deathChunkParticle;
        [SerializeField] private GameObject deathBloodParticle;
        private Rigidbody2D aliveRb;
        private Animator aliveAnim;

        private void Start()
        {
            alive = transform.Find("Alive").gameObject;
            aliveRb = alive.GetComponent<Rigidbody2D>();
            aliveAnim = alive.GetComponent<Animator>();

            currentHp = maxHp;
            facingDirection = 1;
        }

        private void Update()
        {
            switch (currentState)
            {
                case State.Walking:
                    UpdateWalkingState();
                    break;
                case State.Knockback:
                    UpdateKnockBackState();
                    break;
                case State.Dead:
                    UpdateDeadState();
                    break;
            }
        }
        private void EnterWalkingState()
        {

        }

        private void UpdateWalkingState()
        {
            groundDetected = Physics2D.Raycast(groundCheck.position, Vector2.down, groundCheckDistance, isGround);
            

            if(!groundDetected )
            {
                Flip();
            }
            else
            {
                movement.Set(movementSpeed * facingDirection, aliveRb.velocity.y);
                aliveRb.velocity = movement;
            }
        }

        private void ExitWakingState()
        {

        }

        private void EnterKnockBackState()
        {
            knockbackStartTime = Time.time;
            movement.Set(knockBackSpeed.x * damageDirection, knockBackSpeed.y);
            aliveRb.velocity = movement;
            aliveAnim.SetBool("Knockback", true);
        }

        private void UpdateKnockBackState()
        {
            if(Time.time >= knockbackStartTime + knockBackDuration)
            {
                SwitchState(State.Walking);
            }
        }

        private void ExitKnockBackState()
        {
            aliveAnim.SetBool("Knockback", false);
        }

        private void EnterDeadState()
        {
            SoundManager.PlaySound("enemyDie");
            Instantiate(deathChunkParticle, alive.transform.position, deathChunkParticle.transform.rotation);
            Instantiate(deathBloodParticle, alive.transform.position, deathBloodParticle.transform.rotation);
            Destroy(gameObject);
            Debug.Log("Enemy die");
            GameManager.GameManager.MyInstance.AddScore(scoreValue);
        }

        private void UpdateDeadState()
        {

        }

        private void ExitDeadState()
        {

        }

        public void Damage(float[] attackDetails)
        {
            currentHp -= attackDetails[0];
            
            if(attackDetails[1] > alive.transform.position.x)
            {
                damageDirection = -1;
            }
            else
            {
                damageDirection = 1;
            }
        

            if(currentHp > 0.0f)
            {
                SwitchState(State.Knockback);
            }
            else if(currentHp <= 0.0f )
            {
                SwitchState(State.Dead);
            }
        }
        

        private void Flip()
        {
            facingDirection *= -1;
            alive.transform.Rotate(0.0f, 180.0f, 0.0f);
        }
    

        private void SwitchState(State state)
        {
            switch (currentState)
            {
                case State.Walking:
                    ExitWakingState();
                    break;
                case State.Knockback:
                    ExitKnockBackState();
                    break;
                case State.Dead:
                    ExitDeadState();
                    break;
            }

            switch (state)
            {
                case State.Walking:
                    EnterWalkingState();
                    break;
                case State.Knockback:
                    EnterKnockBackState();
                    break;
                case State.Dead:
                    EnterDeadState();
                    break;
            }

            currentState = state;
        }

    

    }
}
