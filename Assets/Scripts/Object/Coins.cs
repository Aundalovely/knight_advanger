﻿using Sound;
using UnityEngine;

namespace Object
{
    public class Coins : Collectable
    {
        [SerializeField] private int scoreValue = 1;

        protected override void Collected()
        {
            SoundManager.PlaySound("getCoin");
            GameManager.GameManager.MyInstance.AddScore(scoreValue);
            Destroy(this.gameObject);
        }

    }
}
