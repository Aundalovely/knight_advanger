﻿using Sound;
using UnityEngine;

namespace Object
{
    public class LevelTwo : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Player")
            {
                GameManager.GameManager.MyInstance.LevelTwo();
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.tag == "Player")
            {
                UIManager.UIManager.MyInstance.HideVictoryCondition();
            }
        }
    
    }
}

