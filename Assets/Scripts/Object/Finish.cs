﻿using System.Collections;
using System.Collections.Generic;
using Sound;
using UnityEngine;

public class Finish : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            GameManager.GameManager.MyInstance.Finish();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            SoundManager.PlaySound("wrong");
            UIManager.UIManager.MyInstance.HideVictoryCondition();
        }
    }
}
