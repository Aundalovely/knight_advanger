﻿using UnityEngine;

namespace Object
{
    public class SpinningSaw : MonoBehaviour
    {
        [SerializeField] private int spinningSpeed ;
        void Update () {
            transform.Rotate (Vector3.forward * spinningSpeed);
        }
    }
}
