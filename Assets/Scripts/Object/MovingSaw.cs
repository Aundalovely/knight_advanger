﻿using Sound;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Object
{
    public class MovingSaw : MonoBehaviour
    {
        [SerializeField] private float speed ;
        [SerializeField] private int maxLimite ;
        [SerializeField] private bool isVerticalMovement;
        [SerializeField] private bool isPlateformMoving;
        private float sawInitialPosition;
        
        void Start () 
        {
            if (isVerticalMovement)
            {
                sawInitialPosition = transform.position.y;
            } 
            else 
            {
                sawInitialPosition = transform.position.x;
            }
        }
        
        void Update () 
        {
            if (isPlateformMoving) 
            {
                movePlateform ();
            }
        }

        void movePlateform()
        {
            Vector3 templateFormPosition = this.transform.position;
            if (isVerticalMovement)
            {
                limitReached (templateFormPosition.y);
                templateFormPosition.y += speed;
            } 
            else 
            {
                limitReached (templateFormPosition.x);
                templateFormPosition.x += speed;
            }

            this.transform.position = templateFormPosition;
        }

        void limitReached(float plateformPosition)
        {
            if ((plateformPosition >= (sawInitialPosition + maxLimite)) || ((plateformPosition <= (sawInitialPosition - maxLimite))))
            {
                speed *= -1;
            }
        }
        
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Player")
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }
}
