﻿using Sound;
using TMPro;
using UnityEngine;

namespace UIManager
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreTxt, victoryConditionTxt;
        [SerializeField] private GameObject victoryCondition;
        private static UIManager instance;
        private void Awake()
        {
            if (instance == null )
            {
                instance = this;
            }

            else
            {
                Destroy(this);
            }
        }

        public static UIManager MyInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UIManager();
                }
                return instance;
            }
        }

        public void SoreUI(int _score, int _victoryCondition)
        {
            scoreTxt.text = "Score: " + _score + " / " + _victoryCondition;
        }

        public void ShowVictoryCondition(int _score, int _victoryCondition)
        {
            victoryCondition.SetActive(true);
            victoryConditionTxt.text = "You need " + (_victoryCondition - _score) + " more Score";
        }
    
        public void HideVictoryCondition()
        {
            SoundManager.PlaySound("wrong");
            victoryCondition.SetActive(false);
        }
    }
}
