﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UIManager
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private GameObject optionPanel;
        [SerializeField] private GameObject mainMenuPanel;

        private void StartGame()
        {
            SceneManager.LoadScene("Level1");
        }

        private void ShowOptions()
        {
            mainMenuPanel.SetActive(false);
            optionPanel.SetActive(true);
        }

        private void ShowMainMenu()
        {
            mainMenuPanel.SetActive(true);
            optionPanel.SetActive(false);
        }

        private void Exit()
        {
            Debug.Log("Exit");
            Application.Quit();
        }
    }
}
