﻿using System;
using UnityEngine;

namespace Sound
{
    public class SoundManager : MonoBehaviour
    {
        private static AudioClip jumpSound, enemyDieSound, playerDieSound, getCoinSound;
        private static AudioClip dashSound, wrongSound,finishSound,hitSound,clickSound;
        private static AudioSource audioSrc;

        private void Start()
        {
            jumpSound = Resources.Load<AudioClip>("jump");
            enemyDieSound = Resources.Load<AudioClip>("enemyDie");
            playerDieSound = Resources.Load<AudioClip>("playerDie");
            getCoinSound = Resources.Load<AudioClip>("getCoin");
            finishSound = Resources.Load<AudioClip>("finishLevel");
            hitSound = Resources.Load<AudioClip>("hit");
            clickSound = Resources.Load<AudioClip>("click");
            dashSound = Resources.Load<AudioClip>("dash");
            wrongSound = Resources.Load<AudioClip>("wrong");

            audioSrc = GetComponent<AudioSource>();
        }

        public static void PlaySound(string clip)
        {
            switch (clip)
            {
                case "jump":
                    audioSrc.PlayOneShot(jumpSound);
                    break;
                case "enemyDie":
                    audioSrc.PlayOneShot(enemyDieSound);
                    break;
                case "playerDie":
                    audioSrc.PlayOneShot(playerDieSound);
                    break;
                case "getCoin":
                    audioSrc.PlayOneShot(getCoinSound);
                    break;
                case "finishLevel":
                    audioSrc.PlayOneShot(finishSound);
                    break;
                case "hit":
                    audioSrc.PlayOneShot(hitSound);
                    break;
                case "click":
                    audioSrc.PlayOneShot(clickSound);
                    break;
                case "dash":
                    audioSrc.PlayOneShot(dashSound);
                    break;
                case "wrong":
                    audioSrc.PlayOneShot(wrongSound);
                    break;

            }
        }
    }
}
